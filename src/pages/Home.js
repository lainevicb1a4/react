import { Fragment } from "react";
import Banner from "../components/Banner";
import BestSeller from "../components/BestSeller";
import ViewProducts from "../components/ViewProducts";

export default function Home() {
  return (
    <Fragment>
      <Banner />
      <BestSeller />
      <ViewProducts />
    </Fragment>
  );
}
