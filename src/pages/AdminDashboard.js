import { Button, Col, Row, Container, Table } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate, Link } from "react-router-dom";

function AdminDashboard() {
  // const { name, brand, description, price, productType, stock } = productProp;
  const { user } = useContext(UserContext);

  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:4000/products/list`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setProducts(data);
      });
  }, []);

  return user.isAdmin !== true ? (
    <Navigate to="*" />
  ) : (
    <div className="p-4">
      <Row className="justify-content-center text-center">
        <h2>Admin Dashboard</h2>
        <div>
          <Button as={Link} to={`/addproduct`} variant="primary">
            Add New Product
          </Button>
          {/* <Button variant="success">Show User Orders</Button> */}
        </div>
        <Table striped bordered hover className="mt-2 ">
          <thead>
            <tr>
              <th>Name</th>
              <th>Brand</th>
              <th>Description</th>
              <th>Price</th>
              <th>Type</th>
              <th>Stock</th>
              <th>Active</th>
              <th>Features</th>
            </tr>
          </thead>
          <tbody>
            {products.map((product) => {
              return (
                <tr key={product._id}>
                  <td>{product.name}</td>
                  <td>{product.brand}</td>
                  <td>{product.description}</td>
                  <td>PHP {product.price}</td>
                  <td>{product.productType}</td>
                  <td>{product.stock} stock(s)</td>
                  <td>{String(product.isActive)}</td>
                  <td>
                    {product.isActive ? (
                      <Button
                        as={Link}
                        to={`/archive/${product._id}`}
                        variant="outline-danger"
                      >
                        Archive
                      </Button>
                    ) : (
                      <Button
                        as={Link}
                        to={`/unarchive/${product._id}`}
                        variant="outline-success"
                      >
                        Unarchive
                      </Button>
                    )}

                    <Button
                      as={Link}
                      to={`/updateProducts/${product._id}`}
                      variant="outline-primary"
                    >
                      Update
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Row>
    </div>
  );
}

export default AdminDashboard;
