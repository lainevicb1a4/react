import { useParams } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

export default function AddToCart() {
  const back = useNavigate();
  const { orderId } = useParams();
  const [quantity, setQuantity] = useState("");
  const [productId1, setProductId1] = useState("");

  const addToCart1 = (event) => {
    event.preventDefault();

    setProductId1(localStorage.getItem("productId"));

    fetch(`http://localhost:4000/orders/addToCartNext/${orderId}`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        productId: productId1,
        quantity: quantity,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        localStorage.setItem("orderId", data._id);
        if (data.userId !== null) {
          Swal.fire({
            title: "Checkout Complete!",
            icon: "success",
            text: "You successfully purchase the product.",
          });

          back(`/checkout/${localStorage.getItem("orderId")}`);
        } else {
          Swal.fire({
            title: "Checkout Failed!",
            icon: "error",
            text: "There's an error for purchasing product, try again.",
          });
          back("/productview");
        }
      });
  };

  return (
    <Form onSubmit={addToCart1} className="mt-2 p-5">
      <Form.Group className="mb-3" controlId="formBasicEmail">
        {/* <h2>{name}</h2> */}
        {/* <h3>{productId}</h3> */}
        <Form.Label>Quantity: </Form.Label>
        <Form.Control
          type="number"
          placeholder="ex. 1"
          value={quantity}
          onChange={(event) => setQuantity(event.target.value)}
          required
        />
        <Button variant="success" type="submit">
          Purchase
        </Button>
      </Form.Group>
    </Form>
  );
}
