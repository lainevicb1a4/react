import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function Signout() {
  const { setUser, unSetUser } = useContext(UserContext);

  unSetUser();

  useEffect(() => {
    setUser({ id: null, isAdmin: false });
  }, []);

  Swal.fire({
    title: "Sign Out successfully!",
    icon: "info",
    text: "You can sign in again.",
  });

  return <Navigate to="/signin" />;
}
