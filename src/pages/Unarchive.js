import { Navigate, useParams } from "react-router-dom";
import { useEffect } from "react";
import Swal from "sweetalert2";

export default function Unarchive() {
  const { productId } = useParams();

  useEffect(() => {
    fetch(`http://localhost:4000/products/unarchive/${productId}`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.unarchieveDone) {
          Swal.fire({
            title: "Product Unarchive",
            icon: "success",
            text: "You successfully unarchive the product.",
          });
        } else {
          Swal.fire({
            title: "Failed to unarchive product",
            icon: "error",
            text: "There's an error in unarchiving product.",
          });
        }
      });
  });
  return <Navigate to="/adminDashboard" />;
}
