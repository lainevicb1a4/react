import ProductsCard from "../components/ProductsCard";
import { Fragment, useEffect, useState } from "react";

export default function ProductView() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:4000/products/viewActive`)
      .then((response) => response.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            return <ProductsCard key={product._id} prop={product} />;
          })
        );
      });
  });
  return (
    <Fragment>
      <h2 className="sub-title mt-3">Skincare Products</h2>
      {products}
    </Fragment>
  );
}
