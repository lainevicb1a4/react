import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import logo from "../imgs/exocare_logo.png";
import { Container, Row, Col } from "react-bootstrap";

import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

import Swal from "sweetalert2";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(true);

  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    if (username !== "" && password !== "") {
      setIsActive(false);
    } else {
      setIsActive(true);
    }
  }, [username, password]);

  function loginUser(event) {
    event.preventDefault();

    fetch(`http://localhost:4000/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        if (data.accessToken !== "empty") {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to our website!",
          });
        } else {
          Swal.fire({
            title: "Authentication failed!",
            icon: "error",
            text: "Check your login details and try again.",
          });
        }
      });

    const retrieveUserDetails = (token) => {
      fetch(`http://localhost:4000/users/profile`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);

          setUser({ id: data._id, isAdmin: data.isAdmin });
        });
    };
  }

  if (user.isAdmin === true) {
    return <Navigate to="/adminDashboard" />;
  } else if (user.isAdmin === false && user.id !== null) {
    return <Navigate to="/productview" />;
  } else {
    return (
      <Container>
        <Row>
          <Col className="mt-5" xs={12} md={6}>
            <img className="product-image" src={logo} alt="exocare_logo" />
          </Col>
          <Col className="mt-5 border-login" xs={12} md={6}>
            <Form onSubmit={loginUser} className="p-3">
              <h3>
                <strong> Sign In </strong>
              </h3>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Username:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Username"
                  value={username}
                  onChange={(event) => setUsername(event.target.value)}
                  required
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password:</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                  required
                />
              </Form.Group>

              <Button variant="success" type="submit" disabled={isActive}>
                Sign In
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}
