import { Fragment } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

export default function BestSeller() {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 4,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 3,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };

  return (
    <div className="mb-1 pb-2 border-login">
      <h2 className="sub-title mt-2">
        🔥<strong>Best Seller</strong>🔥
      </h2>
      <Carousel responsive={responsive}>
        <div className="card1">
          <img
            className="product-image"
            src="https://media.allure.com/photos/59023bd57da179416ac6b43c/master/pass/allure-rca-2017-cetaphil-cleanser-review.jpg"
            width={200}
            height={200}
            alt="product"
          />
          <h6 className="cardText1">Cetaphil Cleanser</h6>
          {/* <p className="price">₱20.99</p>
          <p>some description</p>
          <p>
            <button>Add to cart</button> 
          </p>*/}
        </div>

        <div className="card1">
          <img
            className="product-image"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRoPpYkFedD-qP3Fw8vGk4olUmvxZ6Jmupo8ogSk2pEb7QaYzcLwww8LN8x1VLKgAWd_BY&usqp=CAU"
            width={200}
            height={200}
            alt="product"
          />
          <h6 className="cardText1">Niacinamide Serum</h6>
        </div>

        <div className="card1">
          <img
            className="product-image1"
            src="https://static.chemistwarehouse.com.au/ams/media/pi/91328/2DF_800.jpg"
            width={200}
            height={200}
            alt="product"
          />
          <h6 className="cardText1">Moisturising Cream</h6>
        </div>

        <div className="card1">
          <img
            className="product-image"
            src="https://api.watsons.com.ph/medias/prd-front-10084008.jpg?context=bWFzdGVyfGltYWdlc3wxMjQzMjd8aW1hZ2UvanBlZ3xoY2QvaDcxLzkwMTExMjUzNTQ1MjYvV1RDUEgtMTAwODQwMDgtZnJvbnQuanBnfDg0YjM4Mjk4MWQyMzJjODNhZTVjZGZkYTNiOTU1NWE1OTMyZmM5MjNhZGNjNjRiNmI5OWRlYTdjMjYyODIzZmU"
            width={200}
            height={200}
            alt="product"
          />
          <h6 className="cardText1">Belo Sunscreen</h6>
        </div>

        <div className="card1">
          <img
            className="product-image"
            src="https://ipcdn.freshop.com/resize?url=https://images.freshop.com/1564405684702553069/499af2e123c572f3f202938f8bcc2e45_large.png&width=512&type=webp&quality=90"
            width={200}
            height={200}
            alt="product"
          />
          <h6 className="cardText1">Pantene Shampoo</h6>
        </div>

        <div className="card1">
          <img
            className="product-image"
            src="http://cdn.shopify.com/s/files/1/0260/6877/9066/products/100340.jpg?v=1622208780"
            width={200}
            height={200}
            alt="product"
          />
          <h6 className="cardText1">Safeguard Soap</h6>
        </div>
      </Carousel>
    </div>
  );
}
