import { Fragment } from "react";
import Card from "react-bootstrap/Card";
//import CardGroup from "react-bootstrap/CardGroup";
import { Row, Col } from "react-bootstrap";
import { useState, useEffect } from "react";

export default function ViewProducts() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:4000/products/viewActive`)
      .then((response) => response.json())
      .then((data) => {
        setProducts(data);
      });
  }, []);

  return (
    <Fragment>
      <h2 className="sub-title mt-2">
        <strong>Skincare Products</strong>
      </h2>
      <Row xs={2} md={4} className="g-4">
        {/* <CardGroup className=" m-3"> */}
        {products.map((product) => {
          return (
            <Col>
              <Card className="cardHeight1">
                {/* <Card.Img variant="top" src="holder.js/100px160" /> */}
                <Card.Body>
                  <Card.Title>{product.name}</Card.Title>
                  <Card.Title> </Card.Title>
                  <Card.Text>Brand: {product.brand}</Card.Text>
                </Card.Body>
                <Card.Footer>
                  <Card.Text>Price: {product.price}</Card.Text>
                </Card.Footer>
              </Card>
            </Col>
          );
        })}
        {/* </CardGroup> */}
      </Row>
    </Fragment>
  );
}
