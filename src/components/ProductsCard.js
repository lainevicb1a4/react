import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import { Card, Col, ListGroup } from "react-bootstrap";

function ProductsCard({ prop }) {
  const { _id, name, brand, price } = prop;

  return (
    <Col className="col-4  offset-4 ">
      <Card border="secondary" className="mt-4 ">
        {/* <Card.Img variant="top" src="holder.js/100px180" /> */}
        <Card.Body>
          <Card.Title className="text-dark">{name}</Card.Title>
          {/* <Card.Text>Brand: {brand}</Card.Text>
        <Card.Text>Price: {price}</Card.Text> */}
        </Card.Body>
        <ListGroup variant="flush">
          <ListGroup.Item>Brand: {brand}</ListGroup.Item>
          <ListGroup.Item>Price: {price}</ListGroup.Item>
        </ListGroup>
        <Button as={Link} to={`/product/${_id}`} variant="primary">
          Details
        </Button>
      </Card>
    </Col>
  );
}

export default ProductsCard;
