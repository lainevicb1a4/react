import { Nav, Navbar } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext } from "react";

export default function Navbar2() {
  const { user } = useContext(UserContext);

  if (user.id === null || user.isAdmin === true) {
    return <Navbar></Navbar>;
  } else {
    return (
      <Navbar bg="light" expand="lg" className="nav-bg-2">
        <Navbar.Brand as={NavLink} to="/">
          {" "}
          Exocare
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="m-auto">
            <Nav.Link as={NavLink} to="/">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/productview">
              Skincare
            </Nav.Link>
            <Nav.Link href="#link1">Sale</Nav.Link>
            <Nav.Link href="#link2">Brand</Nav.Link>
          </Nav>
          <Nav className="me-0">
            <Nav.Item>
              <Nav.Link href="#" disabled>
                Add to cart
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="#2" disabled>
                My purchase
              </Nav.Link>
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
